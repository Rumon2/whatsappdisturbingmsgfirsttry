package Demo;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;  

public class BotTestWhatsapp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "E:\\Selenium Jars and Drivers\\Drivers\\chromedriver\\chromedriver.exe");  
	    
	    WebDriver driver=new ChromeDriver();  
	      
	    driver.get("https://web.whatsapp.com/");  
	    driver.manage().window().maximize();
	    
	    try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
	    
	    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH");
	    LocalDateTime now = LocalDateTime.now();
	    int hr=Integer.parseInt(dtf.format(now));
	    
	    driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[1]/div[3]/div/div[1]/div/label/div/div[2]")).sendKeys("My Jio");
	    driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[1]/div[3]/div/div[1]/div/label/div/div[2]")).sendKeys(Keys.ENTER);
	    if(hr<=12)
	    {
	    	driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[1]/div[4]/div[1]/footer/div[1]/div/div/div[2]/div[1]/div/div[2]")).sendKeys("Hi, Good Morning");
	    	driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[1]/div[4]/div[1]/footer/div[1]/div/div/div[2]/div[1]/div/div[2]")).sendKeys(Keys.ENTER);
	    }
	    else if(hr>=12 && hr<=17)
	    {
	    	driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[1]/div[4]/div[1]/footer/div[1]/div/div/div[2]/div[1]/div/div[2]")).sendKeys("Hi, Good Afternoon");
	    	driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[1]/div[4]/div[1]/footer/div[1]/div/div/div[2]/div[1]/div/div[2]")).sendKeys(Keys.ENTER);
	    }
	    else
	    {
	    	driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[1]/div[4]/div[1]/footer/div[1]/div/div/div[2]/div[1]/div/div[2]")).sendKeys("Hi, Good Evening");
	    	driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[1]/div[4]/div[1]/footer/div[1]/div/div/div[2]/div[1]/div/div[2]")).sendKeys(Keys.ENTER);
	    }
	    
	    driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[1]/div[4]/div[1]/footer/div[1]/div/div/div[2]/div[1]/div/div[2]")).sendKeys("Press 1 to schedule the meeting or Press 2 to cancel the meeting");
		driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[1]/div[4]/div[1]/footer/div[1]/div/div/div[2]/div[1]/div/div[2]")).sendKeys(Keys.ENTER);
		//System.out.println(driver.findElement(By.xpath("//span[contains(text(),'1')]")).getText());
		try {
			Thread.sleep(12000);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
		String rply=driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[1]/div[4]/div[1]/div[3]/div/div[2]/div[3]/div[21]/div/div/div/div[1]/div/span[1]/span")).getText();
		System.out.println(rply);
		if(rply.equals("1"))
		{
			driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[1]/div[4]/div[1]/footer/div[1]/div/div/div[2]/div[1]/div/div[2]")).sendKeys("Get into the Lobby");
	    	driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[1]/div[4]/div[1]/footer/div[1]/div/div/div[2]/div[1]/div/div[2]")).sendKeys(Keys.ENTER);
		}
		else if(rply.equals("2"))
		{
			driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[1]/div[4]/div[1]/footer/div[1]/div/div/div[2]/div[1]/div/div[2]")).sendKeys("meeting schedule is canceled");
	    	driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[1]/div[4]/div[1]/footer/div[1]/div/div/div[2]/div[1]/div/div[2]")).sendKeys(Keys.ENTER);
		}
		else
		{
			driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[1]/div[4]/div[1]/footer/div[1]/div/div/div[2]/div[1]/div/div[2]")).sendKeys("You are Knocked Out!! Get lost!!");
	    	driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[1]/div[4]/div[1]/footer/div[1]/div/div/div[2]/div[1]/div/div[2]")).sendKeys(Keys.ENTER);
		}
	}

}
